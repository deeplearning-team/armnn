//
// Copyright  2017 Arm Ltd. All rights reserved.
// SPDX-License-Identifier: MIT
//
#include <armnn/INetwork.hpp>
#include <armnn/IRuntime.hpp>

#include <iostream>

/// A simple example of using the ArmNN SDK API.
int main()
{
    using namespace armnn;

    // Construct ArmNN network
    INetworkPtr myNetwork = INetwork::Create();
    auto printer = myNetwork.get()->PrintGraph();
    std::cout << printer << std::endl;
    return 0;
}
 